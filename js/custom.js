/*__________ Image Attribute +++ __________*/
var image_text = "Image may contain ";

for (var i = 0; i < $("img").length; i++) {
	var image_attribute = $("img").eq(i).attr("alt");

	$("img").eq(i).attr("alt", image_text + image_attribute);
}
/*__________ Image Attribute --- __________*/


/*__________ Change Language +++ __________*/
$(".hdr_lang_list > li").click(function () {
	var lang_skate = $(".hdr_lang_skate");

	if (lang_skate.hasClass("uz")) {
		lang_skate.removeClass("uz");
		lang_skate.addClass("ru");
		lang_skate.text("ru");
	}
	else {
		lang_skate.removeClass("ru");
		lang_skate.addClass("uz");
		lang_skate.text("uz");
	}
});
/*__________ Change Language --- __________*/


/*__________ Category Dropdown Image +++ __________*/
$(".ctgr_nav_menu > li").mouseover(function () {

	var imageAll = $(this).find("img");
	for (var i = 0; i < imageAll.length; i++) {
		var imageURL = imageAll.eq(i).attr("custom-src");
		imageAll.eq(i).removeAttr("custom-src");
		imageAll.eq(i).attr("src", imageURL);
	}

});
/*__________ Category Dropdown Image --- __________*/


/*__________ Owl Carousel +++ __________*/
$(document).ready(function () {
	try {
		var owl = $(".baner");

		owl.owlCarousel({
			autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".baner_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".baner_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".hot_offer_baner");

		owl.owlCarousel({
			autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: false,
			itemsCustom: [
				[751, 2],
				[975, 3],
				[1183, 4]
			],
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".hot_offer_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".hot_offer_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .hot_offer_baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".new_prod_menu");

		owl.owlCarousel({
			itemsCustom: [
				[751, 3],
				[1183, 4]
			],
			navigation: false,
			pagination: false
		});

		// Custom Navigation Events
		$(".new_prod_menu_btn.next").click(function () {
			owl.trigger('owl.next');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .new_prod_menu cannot find elements");
	}
});


$(document).ready(function () {
	try {
		$(".rka_main_md_bnr").owlCarousel({
			autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			// transitionStyle : "fade",
			lazyLoad: true
		});
	}
	catch (e) {
		console.warn("Owl Carousel .rka_main_md_bnr cannot find elements");
	}
});


// $(document).ready(function () {
// 	try {
// 		var owl = $(".beauty_baner");

// 		owl.owlCarousel({
// 			autoPlay : 3000,
// 			stopOnHover: true,
// 			navigation: false,
// 			pagination: false,
// 			itemsCustom: [
// 				[751, 2],
// 				[975, 3],
// 				[1183, 4]
// 			],
// 			lazyLoad: true
// 		});

// 		// Custom Navigation Events
// 		$(".beauty_bnr_btn.next").click(function(){
// 			owl.trigger('owl.next');
// 		});

// 		$(".beauty_bnr_btn.prev").click(function(){
// 			owl.trigger('owl.prev');
// 		});
// 	}
// 	catch(e) {
// 		console.warn("Owl Carousel .beauty_baner cannot find elements");
// 	}
// });


// $(document).ready(function () {
// 	try {
// 		var owl = $(".furniture_baner");

// 		owl.owlCarousel({
// 			autoPlay : 3000,
// 			stopOnHover: true,
// 			navigation: false,
// 			pagination: false,
// 			itemsCustom: [
// 				[751, 2],
// 				[975, 3],
// 				[1183, 4]
// 			],
// 			lazyLoad: true
// 		});

// 		// Custom Navigation Events
// 		$(".furniture_bnr_btn.next").click(function(){
// 			owl.trigger('owl.next');
// 		});

// 		$(".furniture_bnr_btn.prev").click(function(){
// 			owl.trigger('owl.prev');
// 		});
// 	}
// 	catch(e) {
// 		console.warn("Owl Carousel .furniture_baner cannot find elements");
// 	}
// });


$(document).ready(function () {
	try {
		var owl = $(".brand_baner");

		owl.owlCarousel({
			autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: false,
			itemsCustom: [
				[751, 3],
				[1183, 4]
			],
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".brand_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".brand_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .brand_baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".cAln_ctgr_baner");

		owl.owlCarousel({
			autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: false,
			itemsCustom: [
				[751, 2]
			],
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".cAln_ctgr_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".cAln_ctgr_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .cAln_ctgr_baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".prods_rcm_baner");

		owl.owlCarousel({
			autoPlay : 3000,
			stopOnHover: true,
			navigation: false,
			pagination: false,
			itemsCustom: [
				[751, 1],
				[975, 2],
				[1183, 3]
			],
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".prods_rcm_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".prods_rcm_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .prods_rcm_baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".prod_info_desc_bnr");

		owl.owlCarousel({
			autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			lazyLoad: true
		});

		// Custom Navigation Events
		$(".prod_info_desc_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".prod_info_desc_bnr_btn.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .prod_info_desc_bnr cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".pc_my_trd_ftr_bnr");

		owl.owlCarousel({
			itemsCustom: [
				[751, 3],
				[1183, 4]
			],
			navigation: false,
			pagination: false
		});

		// Custom Navigation Events
		$(".pc_my_trd_ftr_bnr_btn.next").click(function () {
			owl.trigger('owl.next');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .new_prod_menu cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".shp_sell_bnr");

		owl.owlCarousel({
			autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			lazyLoad: true
		});
	}
	catch (e) {
		console.warn("Owl Carousel .baner cannot find elements");
	}
});


$(document).ready(function () {
	try {
		var owl = $(".prod_mdl_rcm_bnr.b1");

		owl.owlCarousel({
			autoPlay: 3000,
			stopOnHover: true,
			itemsCustom: [
				[751, 2],
				[1183, 3]
			],
			navigation: false,
			pagination: false,
		});

		// Custom Navigation Events
		$(".prod_mdl_rcm_bnr_btn.b1.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".prod_mdl_rcm_bnr_btn.b1.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .prod_mdl_rcm_bnr cannot find elements");
	}
});

$(document).ready(function () {
	try {
		var owl = $(".prod_mdl_rcm_bnr.b2");

		owl.owlCarousel({
			autoPlay: 3000,
			stopOnHover: true,
			itemsCustom: [
				[751, 2],
				[1183, 3]
			],
			navigation: false,
			pagination: false,
		});

		// Custom Navigation Events
		$(".prod_mdl_rcm_bnr_btn.b2.next").click(function () {
			owl.trigger('owl.next');
		});

		$(".prod_mdl_rcm_bnr_btn.b2.prev").click(function () {
			owl.trigger('owl.prev');
		});
	}
	catch (e) {
		console.warn("Owl Carousel .prod_mdl_rcm_bnr cannot find elements");
	}
});

/*__________ Owl Carousel --- __________*/


/*__________ Fixed Search +++ __________*/
var offset = $('.hdr_logo_sect_inner').offset().top;
var searchHeight = $('.hdr_logo_sect_inner').outerHeight();

$(window).scroll(function () {
	if ($(window).scrollTop() >= offset + searchHeight) {
		$('.hdr_logo_sect_inner').addClass('active');
	}
	else if ($(window).scrollTop() <= offset) {
		$('.hdr_logo_sect_inner').removeClass('active');
	}
});
/*__________ Fixed Search --- __________*/


/*__________ Masonry +++ __________*/
$(window).on("load", function () {
	try {
		$('.ctgr_all_cnt').masonry({
			itemSelector: '.ctgr_all_col',
			horizontalOrder: true
		});
	}
	catch (e) {
		console.warn("Masonry cannot find elements");
	}
});
/*__________ Masonry --- __________*/


/*__________ About Menu +++ __________*/
$(".about_menu > li > a").click(function () {

	var i = $(this).parent().index();
	$('.about_text').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	return false;
});
/*__________ About Menu --- __________*/


/*__________ Help +++ __________*/
$(".help_quest_title").click(function () {
	$(this).closest(".help_quest").toggleClass('active')
	$(this).closest(".help_quest").find('.help_quest_body').slideToggle();
});
/*__________ Help --- __________*/


/*__________ Registration +++ __________*/
// $(".reg_select_rdo > input").click(function () {
// 	if ($(this).parent().hasClass("person"))
// 	{
// 		$(".reg_form_box").removeClass("active");
// 		$(".reg_form_box.person").addClass("active");
// 		$(".reg_aside").removeClass("hidden");
// 	}
// 	else if ($(this).parent().hasClass("enterprise"))
// 	{
// 		$(".reg_form_box").removeClass("active");
// 		$(".reg_form_box.enterprise").addClass("active");
// 		$(".reg_aside").addClass("hidden");
// 	}
// });
/*__________ Registration --- __________*/


/*__________ Personal Cabinet Information +++ __________*/
$(".pc_info_change_pass_btn > a").click(function () {
	$(this).parent().addClass("hidden");
	$(".pc_info_change_pass_hr, .pc_info_change_pass").removeClass("hidden");

	return false;
});
/*__________ Personal Cabinet Information --- __________*/


/*__________ Personal Cabinet History +++ __________*/
$(".pc_hstr_menu > li > a").click(function () {

	var i = $(this).parent().index();
	$('.pc_hstr_table_box').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	return false;
});
/*__________ Personal Cabinet History --- __________*/


/*__________ Cart Control Input +++ __________*/
$(".cart_item_amount_inp").keyup(function () {

	$(this).closest(".cart_item_amt_form").find(".cart_item_amt_btn_sub").addClass("active");

	var checkValue = parseInt($(this).val());

	if (isNaN(checkValue) || checkValue == 0) {
		$(this).val(1);
	}
	else {
		$(this).val(checkValue);
	}
});
/*__________ Cart Control Input --- __________*/


/*__________ Cart Control Button +++ __________*/
$(".cart_item_amt_ctrl_btn").click(function () {

	$(this).closest(".cart_item_amt_form").find(".cart_item_amt_btn_sub").addClass("active");

	var amountInput = $(this).siblings(".cart_item_amount_inp");
	var amount = parseInt(amountInput.val());
	if ($(this).hasClass("minus")) {
		if (amount > 1) {
			amountInput.val(--amount);
		}
	}
	else if ($(this).hasClass("plus")) {
		amountInput.val(++amount);
	}

});
/*__________ Cart Control Button --- __________*/


/*__________ Cart Submit Button +++ __________*/
$(".cart_item_amt_btn_sub").click(function () {
	$(this).removeClass("active");

	return false;
});
/*__________ Cart Submit Button --- __________*/


/*__________ Cart Coupon +++ __________*/
$(".cart_cpn_title").click(function () {
	$(this).parent().toggleClass("active");
});
/*__________ Cart Coupon --- __________*/


/*__________ Products Filter +++ __________*/
$(".prods_fltr_title").click(function () {
	$(this).parent().toggleClass("inactive");

	$(this).siblings('.prods_fltr_cnt').slideToggle();
});
/*__________ Products Filter --- __________*/


/*__________ Products Filter Checkbox +++ __________*/
$(".prods_fltr_check_more > button").click(function () {
	$(this).parent().siblings(".prods_fltr_check.hidden").removeClass("hidden");

	$(this).parent().addClass("hidden");
});
/*__________ Products Filter Checkbox --- __________*/


/*__________ Products Option Select +++ __________*/
$(document).on("click", ".prods_opt_sel_title", function () {
	$(this).next().toggleClass("active");
});

$(document).click(function (e) {
	if ($(e.target).is(".prods_opt_sel, .prods_opt_sel *") === false) {
		$(".prods_opt_sel_cnt").removeClass("active");
	}
});

// $(".prods_opt_sel_list > li > a").click(function () {
// 	$(".prods_opt_sel_chosen").html($(this).html());
//	
// 	$(".prods_opt_sel_cnt").removeClass("active");
//
// 	return false;
// });
/*__________ Products Option Select --- __________*/


/*__________ Products Type +++ __________*/
$(".prods_type_item").click(function () {

	var i = $(this).index();
	$(".prods_type_box").eq(i).addClass("active").siblings(".prods_type_box").removeClass("active");

	$(this).addClass("active").siblings(".prods_type_item").removeClass("active");

	// return false;

});
/*__________ Products Type --- __________*/


/*__________ Products Pagination +++ __________*/
$(".prods_pgn_go_inp").keyup(function () {

	var checkValue = parseInt($(this).val());

	if (isNaN(checkValue) || checkValue == 0) {
		$(this).val(1);
	}
	else {
		$(this).val(checkValue);
	}
});
/*__________ Products Pagination --- __________*/


/*__________ Elevate Zoom Plus +++ __________*/
try {
	$(".prod_rew_img_zoom").ezPlus({
		gallery: 'thumbImgGroup',
		galleryActiveClass: "active",
		galleryItem: 'a',
		galleryEvent: 'click',
		// galleryEvent: 'mouseover',
		lensBorderColour: '#2cc7b0',
		lensColour: 'rgba(255, 255, 255, 0.4)',
		lensOpacity: 1,
		borderColour: '#2cc7b0',
		borderSize: 3,
		cursor: 'pointer',

		// scrollZoom: true, //allow zoom on mousewheel, true to activate
		// mantainZoomAspectRatio: true,
		// maxZoomLevel: false,
		// minZoomLevel: 1.01,
		// zoomLevel: 1, //default zoom level of image
		//
		// zoomWindowHeight: 300,
		// zoomWindowWidth: 300,
	});
}
catch (e) {
	console.warn("ezPlus cannot find elements");
}

$(".prod_rew_thumb_img").mouseover(function () {
	var imageSource = $(this).data("zoom-image");

	$(".prod_rew_img_inner").attr("href", imageSource);
});
/*__________ Elevate Zoom Plus --- __________*/


/*__________ Fancy Box +++ __________*/
try {
	$().fancybox({
		selector: '[data-fancybox="fancy_img"]',
		loop: true
	});
}
catch (e) {
	console.warn("Fancybox cannot find elements");
}
/*__________ Fancy Box --- __________*/


/*__________ Product Review Amount Input +++ __________*/
$(".prod_rew_amt_ctrl_inp").keyup(function () {

	var checkValue = parseInt($(this).val());

	if (isNaN(checkValue) || checkValue == 0) {
		checkValue = 1;
		$(this).val(checkValue);
	}
	else {
		$(this).val(checkValue);
	}


	// var itemPrice = $(".prod_rew_price>p .val").text();
	// var itemPriceClean = "";
	// for (var i = 0; i < itemPrice.length; i++) {
	// 	if (itemPrice[i] != " ") {
	// 		itemPriceClean += itemPrice[i];
	// 	}
	// }
	//
	// $(".prod_rew_amt_price>p .val").text(parseInt(itemPriceClean) * checkValue);

});
/*__________ Product Review Amount Input --- __________*/


/*__________ Product Review Amount Button +++ __________*/
// $(".prod_rew_amt_ctrl_btn").click(function () {
//
// 	var amountInput = $(this).siblings(".prod_rew_amt_ctrl_inp");
// 	var amount = parseInt(amountInput.val());
// 	if ($(this).hasClass("minus")) {
// 		if (amount > 1) {
// 			amountInput.val(--amount);
// 		}
// 	}
// 	else if ($(this).hasClass("plus")) {
// 		amountInput.val(++amount);
// 	}
//
//
// 	var itemPrice = $(".prod_rew_price>p .val").text();
// 	var itemPriceClean = "";
// 	for (var i = 0; i < itemPrice.length; i++) {
// 		if (itemPrice[i] != " ") {
// 			itemPriceClean += itemPrice[i];
// 		}
// 	}
//
// 	// $(".prod_rew_amt_price>p .val").text(parseInt(itemPriceClean) * amount);
//
// });
/*__________ Product Review Amount Button --- __________*/


/*__________ Product Info Catalog +++ __________*/
$(".prod_info_ctlg_item_title").click(function () {
	$(this).parent().toggleClass("active");
});
/*__________ Product Info Catalog --- __________*/


/*__________ Products Range Slider +++ __________*/
try {
	var rangeSlider = document.getElementsByClassName('prods_range')[0]
	var from = $('#cost_from').val()
	var to = $('#cost_to').val()
	if (!from) {
		from = 0;
	}
	if (to == 0) {
		to = 50000000;
	}

	noUiSlider.create(rangeSlider, {
		start: [from, to],
		step: 1000,
		connect: true,
		range: {
			'min': 0,
			'max': 50000000
		},
		format: wNumb({
			decimals: 0,
			thousand: ' ',
			// postfix: ' UZS'
		})
	})

	rangeSlider.noUiSlider.on('change', function () {
		$('.prods_range_btn').addClass('active')
	})

	var rangeInputFrom = document.getElementsByClassName('prods_range_inp')[0]
	var rangeInputTo = document.getElementsByClassName('prods_range_inp')[1]

	rangeSlider.noUiSlider.on('update', function (values, handle) {

		var value = values[handle]

		if (handle) {
			rangeInputTo.value = value
			val = parseInt(value.replace(/ /gi, ''));
			$('#cost_to').val(val);
		}
		else {
			rangeInputFrom.value = value
			val = parseInt(value.replace(/ /gi, ''));
			$('#cost_from').val(val);
		}
	})

	rangeInputTo.addEventListener('change', function () {
		rangeSlider.noUiSlider.set([null, this.value])
	})

	rangeInputFrom.addEventListener('change', function () {
		rangeSlider.noUiSlider.set([this.value, null])
	})
}
catch (e) {
	console.warn('Range Slider cannot find elements')
}

/*__________ Products Range Slider --- __________*/


/*__________ International Tel Input +++ __________*/
try {
	$(".intl_tel").intlTelInput({
		// allowDropdown: false,
		// autoHideDialCode: false,
		// autoPlaceholder: "off",
		// dropdownContainer: "body",
		// excludeCountries: ["us"],
		// formatOnDisplay: false,
		geoIpLookup: function (callback) {
			$.get("http://ipinfo.io", function () {
			}, "jsonp").always(function (resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		// initialCountry: "auto",
		// nationalMode: false,
		onlyCountries: ['uz', 'ru', 'en', 'kz', 'kg', 'kr'],
		// placeholderNumberType: "MOBILE",
		preferredCountries: ['uz'],
		// separateDialCode: true,
		// utilsScript: "lib/int_tel_input/js/utils.js"
	});
}
catch (e) {
	console.warn("International Tel Input cannot find elements");
}
/*__________ International Tel Input --- __________*/


/*__________ Recovery +++ __________*/
// $(document).on("click", ".pass_r_rdo > input", function () {
// 	$(".pass_r_form_type").removeClass("active");

// 	if ($(this).parent().hasClass("tel"))
// 	{
// 		$(".pass_r_form_type.tel").addClass("active");
// 	}
// 	else
// 	{
// 		$(".pass_r_form_type.email").addClass("active");
// 	}
// });
/*__________ Recovery --- __________*/


/*__________ Vacancy +++ __________*/
$(document).on("click", ".vacancy_job_name > a, .vacancy_ins_back > a", function () {

	if ($(".vacancy_item.home").hasClass("active")) {
		$(".vacancy_item").removeClass("active");
		$(".vacancy_item.inside").addClass("active");
	}
	else {
		$(".vacancy_item").removeClass("active");
		$(".vacancy_item.home").addClass("active");
	}

	return false;
});
/*__________ Vacancy --- __________*/


/*__________ Custom File Input +++ __________*/
$(".vacancy_mod_file_inp > .hdn").change(function () {
	var fileName = $(this).val().split("\\").pop();

	if (fileName == "") {
		$(this).siblings(".vsbl").val("Файл не выбран");
	}
	else {
		$(this).siblings(".vsbl").val(fileName);
	}
});
/*__________ Custom File Input --- __________*/


/*__________ Vacancy +++ __________*/
$(document).on("click", ".comp_news_art_h > a, .comp_news_ins_back > a", function () {

	if ($(".comp_news_item.home").hasClass("active")) {
		$(".comp_news_item").removeClass("active");
		$(".comp_news_item.inside").addClass("active");
	}
	else {
		$(".comp_news_item").removeClass("active");
		$(".comp_news_item.home").addClass("active");
	}

	return false;
});
/*__________ Vacancy --- __________*/


/*__________ Dropify +++ __________*/
try {
	$('.add_prod_pic_add_sm_inp').dropify({
		messages: {
			'default': 'Перетащите файл здесь или нажмите',
			'replace': 'Перетащите или щелкните, чтобы заменить',
			'remove': 'Удалить',
			'error': 'Ooops, что-то не так'
		},
		tpl: {
			preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"></div></div></div>',
			message: '<div class="dropify-message"><div class="dropify_img"><img src="/images/add/photo_upload.png"></div></div>',
			errorsContainer: ''
		}
	});
}
catch (e) {
	console.warn("Dropify cannot find elements");
}


try {
	$('.add_prod_pic_add_lg_inp').dropify({
		messages: {
			'default': 'Перетащите файл здесь или нажмите',
			'replace': 'Перетащите или щелкните, чтобы заменить',
			'remove': 'Удалить',
			'error': 'Ooops, что-то не так'
		},
		tpl: {
			preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"></div></div></div>',
			message: '<div class="dropify-message"><div class="dropify_img"><img src="/images/add/photo_upload.png"></div> <p>{{ default }}</p></div>',
			errorsContainer: ''
		}
	});
}
catch (e) {
	console.warn("Dropify cannot find elements");
}


$(document).on("click", ".add_prod_pic_capt_t_del", function () {
	$(".dropify-clear").click();
});


try {
	$('.pc_b_pd_img_inp').dropify({
		tpl: {
			wrap: '<div class="dropify-wrapper"></div>',
			loader: '',
			message: '<div class="dropify-message"><div class="dropify_img"><img src="/images/pc_seller/pc_pd_man.png"></div></div>',
			preview: '<div class="dropify-preview"><span class="dropify-render"></span></div>',
			filename: '',
			clearButton: '',
			errorLine: '',
			errorsContainer: ''
		}
	});
}
catch (e) {
	console.warn("Dropify cannot find elements");
}


try {
	$('.pc_my_shp_img_it').dropify({
		tpl: {
			wrap: '<div class="dropify-wrapper"></div>',
			loader: '',
			message: '<div class="dropify-message"><div class="dropify_img"><img src="images/add/photo_upload.png"></div></div>',
			preview: '<div class="dropify-preview"><span class="dropify-render"></span></div>',
			filename: '',
			clearButton: '',
			errorLine: '',
			errorsContainer: ''
		}
	});
}
catch (e) {
	console.warn("Dropify cannot find elements");
}
/*__________ Dropify --- __________*/


/*__________ Add Category +++ __________*/
$(document).on("click", ".add_ctgr_opt_lg_ls > li > a", function () {

	$(this).parent().addClass("active").siblings().removeClass("active");

	// Change corresponding list
	$(".add_ctgr_opt_sm_ls").eq($(this).parent().index()).addClass("active").siblings().removeClass("active");

	// Change selected category
	$(".add_ctgr_sel_ls > li.lg").text($(this).children(".txt").text());

	return false;
});


$(document).on("click", ".add_ctgr_opt_sm_ls > li > a", function () {
	$(this).parent().addClass("active").siblings().removeClass("active");

	// Change selected subcategory
	$(".add_ctgr_sel_ls > li.sm").text($(this).text());

	return false;
});
/*__________ Add Category --- __________*/


/*__________ PC Mark +++ __________*/
$(document).on("click", ".pc_mrk_cmt_tab_btn", function () {
	$(this).addClass("active").siblings().removeClass("active");

	$(".pc_mrk_cmt_tab_cnt").eq($(this).index()).addClass("active").siblings().removeClass("active");

	return false;
});
/*__________ PC Mark --- __________*/


/*__________ PC Trade +++ __________*/
$(document).on("click", ".pc_my_trd_ftr_bnr_it", function () {
	$(".pc_my_trd_ftr_bnr_it").removeClass("active");
	$(this).addClass("active");

	$(".pc_my_trd_tb_bx").eq($(this).parent().index()).addClass("active").siblings().removeClass("active");

	return false;
});
/*__________ PC Trade --- __________*/


/*__________ Product Modal +++ __________*/
$(document).on("click", ".prod_mdl_rcm_tab > li > a", function () {
	$(this).parent().addClass("active").siblings().removeClass("active");

	$(".prod_mdl_rcm_bnr_bx").eq($(this).parent().index()).addClass("active").siblings().removeClass("active");

	return false;
});
/*__________ Product Modal --- __________*/


/*__________ Product Info +++ __________*/
$(document).on("click", ".prod_info_desc_menu > li > a", function () {
	$(this).parent().addClass("active").siblings().removeClass("active");

	$(".prod_info_desc_cnt").eq($(this).parent().index()).addClass("active").siblings().removeClass("active");

	return false;
});
/*__________ Product Info --- __________*/


/*__________ Cart Input +++ __________*/
$(document).on("focus", ".bin_it_qwt_inp", function () {
	$(".bin_it_qwt_fm").removeClass("active");
	$(this).closest(".bin_it_qwt_fm").addClass("active");
});

$(document).click(function (e) {
	if ($(e.target).is('.bin_it_qwt_fm, .bin_it_qwt_fm *') === false) {
		$('.bin_it_qwt_fm').removeClass('active');
	}
});
/*__________ Cart Input --- __________*/


/*__________ Cart Control Input +++ __________*/
$(".bin_it_qwt_inp").keyup(function () {

	var checkValue = parseInt($(this).val());

	if (isNaN(checkValue) || checkValue == 0) {
		$(this).val(1);
	}
	else {
		$(this).val(checkValue);
	}
});
/*__________ Cart Control Input --- __________*/


/*__________ Cart Control Button +++ __________*/
$(".bin_it_qwt_btn").click(function () {

	var amountInput = $(this).siblings(".bin_it_qwt_inp");
	var amount = parseInt(amountInput.val());
	if ($(this).hasClass("minus")) {
		if (amount > 1) {
			amountInput.val(--amount);
		}
	}
	else if ($(this).hasClass("plus")) {
		amountInput.val(++amount);
	}

});
/*__________ Cart Control Button --- __________*/


/*__________ Cost Change Button +++ __________*/
$('.prods_range_inp').on('change', function () {
	$('.prods_range_btn').addClass('active');
});
/*__________ Cost Change Button --- __________*/


/*__________ Hot Sale Menu +++ __________*/
$(document).on("click", ".hot_ofr_menu_ls > li > a", function () {
	$(this).parent().toggleClass("active").siblings().removeClass("active");

	return false;
});
/*__________ Hot Sale Menu --- __________*/


/*__________ Custom File Input +++ __________*/
$(".pc_chat_wrt_atch > input").change(function () {
	var fileName = $(this).val().split("\\").pop();

	if (fileName == "") {
		$('.pc_chat_wrt_file > .val').text("Файл не выбран");
	}
	else {
		$('.pc_chat_wrt_file > .val').text(fileName);
	}
});
/*__________ Custom File Input --- __________*/


/*__________ Delivery Product +++ __________*/
$(document).on('click', '.dlvr_pr_drd_ttl', function () {
	$(this).parent().toggleClass('active');
	$(this).nextAll('.dlvr_pr_drd_cnt').slideToggle();
});
/*__________ Delivery Product --- __________*/


/*__________ Order Check +++ __________*/
$(document).on('click', '.ord_i_prd_chk.all > input', function () {
	var checkAllState = $(this).prop('checked');

	if (checkAllState) {
		$('.ord_i_prd_chk.one > input').prop('checked', true);
	}
	else {
		$('.ord_i_prd_chk.one > input').prop('checked', false);
	}

});
/*__________ Order Check --- __________*/


/*__________ Order Tab +++ __________*/
$(document).on('click', '.ord_i_tab_ls > li > a', function () {
	$(this).parent().addClass('active').siblings().removeClass('active');
	$('.ord_i_tab_cnt').eq($(this).parent().index()).addClass('active').siblings().removeClass('active');

	return false;
});
/*__________ Order Tab --- __________*/


/*__________ Buyer Address +++ __________*/
$(document).on('click', '.pc_b_adrs_add > li > a', function (e) {
	e.preventDefault();

	$('.pc_b_adrs_list').removeClass('active');
	$('.pc_b_adrs_form').addClass('active');

});

$(document).on('click', '.pc_b_adrs_fl_btn_ls > li > a.cancel', function (e) {
	e.preventDefault();

	$('.pc_b_adrs_form').removeClass('active');
	$('.pc_b_adrs_list').addClass('active');

});
/*__________ Buyer Address --- __________*/


/*__________ Personal Data +++ __________*/
$(document).on('click', '.pc_b_pd_view_edit', function (e) {
	e.preventDefault();

	$('.pc_b_pd_view').removeClass('active');
	$('.pc_b_pd_edit').addClass('active');
});

$(document).on('click', '.pc_b_pd_edit_cancel', function (e) {
	e.preventDefault();

	$('.pc_b_pd_edit').removeClass('active');
	$('.pc_b_pd_view').addClass('active');
});
/*__________ Personal Data --- __________*/


/*__________ Modal Seller Error +++ __________*/
$(document).on('click', '[data-target="#modSlrErr"]', function () {
	return false;
});
/*__________ Modal Seller Error --- __________*/









/*__________ Reklama Page Tracker +++ __________*/
$(document).on('click', '.rka_p_tb tbody tr', function () {

	$('.rka_p_img_ls > li').removeClass('active').eq($(this).index()).addClass('active');

	var imageTopPosition = $('.rka_p_img_ls > li').eq($(this).index()).offset().top;

	$('html, body').animate({ scrollTop: imageTopPosition - 100 }, 700);

});


// $(document).click(function (e) {
// 	if ($(e.target).is('.rka_p_tb, .rka_p_tb *') === false) {
// 		$('.rka_p_img_ls > li').removeClass('active');
// 	}
// });

/*__________ Reklama Page Tracker --- __________*/



















/*__________ Validation +++ __________*/

try {

}
catch (e) {
	console.warn("Owl Carousel cannot find elements");
}